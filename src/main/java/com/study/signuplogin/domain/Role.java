package com.study.signuplogin.domain;

public enum Role {
	ADMIN("ROLE_ADMIN"),
	MEMBER("ROLE_MEMBER");
	
	private String value;
	
	Role(String v){
		value = v;
	}
	public String getValue() {
		return value;
	}
}
