package com.study.signuplogin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.study.signuplogin.dto.MemberDto;
import com.study.signuplogin.service.MemberService;

import lombok.AllArgsConstructor;

@Controller
@AllArgsConstructor
public class MemberController {
	@Autowired private MemberService memberService;
	
	@RequestMapping(value = "/")
	public String index() { 
		return "/index";
	}
	
	@GetMapping("/user/signup")
	public String dispsignup() {
		return "/signup";
	}
	
	@PostMapping("/user/signup")
	public String execSignup(MemberDto memberDto) {
		memberService.joinUser(memberDto);
		System.out.println("test");
		return "redirect:/user/login";
	}
	
	@RequestMapping("/user/login")
	public String dispLogin() {
		return "/login";
	}
	
	@RequestMapping("/user/login/result")
	public String dispLoginResult() {
		return "/loginSuccess";
	}
	
	@RequestMapping("/user/logout/result")
	public String dispLogout() {
		return "/logout";
	}
	
	@RequestMapping("user/denied")
	public String dispDenied() {
		return "/denied";
	}
	
	@RequestMapping("/user/info")
	public String dispMyInfo() {
		return "/myinfo";
	}
	
	@RequestMapping("/admin")
	public String dispAdmin() {
		return "/admin";
	}
}

