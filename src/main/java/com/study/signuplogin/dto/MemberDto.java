package com.study.signuplogin.dto;

import java.time.LocalDateTime;

import com.study.signuplogin.domain.entity.MemberEntity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
public class MemberDto {
	private Long id;
	private String email;
	private String password;
	private LocalDateTime createdDate;
	private LocalDateTime modifiedDate;
	
	public MemberEntity toEntity() {
		return new MemberEntity(id,email,password);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(LocalDateTime modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Builder
	public MemberDto(Long id, String email, String password) {
		this.id = id;
		this.email = email;
		this.password = password;
	}
}
